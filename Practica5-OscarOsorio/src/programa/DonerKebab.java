package programa;

import java.util.Scanner;

import clases.GestorPedidos;

public class DonerKebab {

	public static void main(String[] args) {
		Scanner input= new Scanner(System.in);
		//creo variables
		int select = 0;
		//1- Crear instancia de GestorClases. 
		GestorPedidos gestor= new GestorPedidos();
		do {
			System.out.println("**************************************");
            System.out.println("               Menu              ");
            System.out.println("1.- Dar de alta 3 elementos de Clase1 y listar.");
            System.out.println("2.- Buscar un elemento de Clase1.");
            System.out.println("3.- Eliminar un elemento de Clase1 y listar.");
            System.out.println("4.- Dar de alta 3 elementos de Clase2 y listar.");
            System.out.println("5.- Buscar un elemento de Clase2.");
            System.out.println("6.- Eliminar un elemento de Clase2 y listar.");
            System.out.println("7.- Lista elementos Clase2 por algun atributo.");
            System.out.println("8.- Listar elementos Clase2 con un elemento de Clase1.");
            System.out.println("9.- Asignar un elemento que use Clase1 y Clase2 y listar.");
            System.out.println("10.- Metodo extra Comprobar los pedidos realizados");
            System.out.println("11.- Metodo extra Ver el nivel del cliente");
            System.out.println("12.- Metodo extra Ver si nos toca un kebab gratis");
            System.out.println("13.- Metodo extra Canjear codigo");
            System.out.println("14.- Salir");
            System.out.println("***************************************");
            
            select=input.nextInt();
		switch (select) {
		case 1:
			//2- Dar de alta 3 elementos de Clase1 y listar. 
			gestor.altaCliente( "Alberto","854226125", "Las Marianas", "36");
			gestor.altaCliente( "Pedro","976517982", "El Quijote", "2");
			gestor.altaCliente( "Elena","985456845", "Pirineos", "52");
			gestor.listaClientes();			
			break;
		case 2:
			//3- Buscar un elemento de Clase1.
			System.out.println(gestor.buscarCliente("976517982"));
			break;
		case 3:
			//4- Eliminar un elemento de Clase1 y listar.
			gestor.eliminarCliente("976517982");
			gestor.listaClientes();
			break;
		case 4:
			//5- Dar de alta 3 elementos de Clase2 y listar.
			gestor.altaProducto("1", "XL", "no", "si", "mixto", "no", "si", "no");
			gestor.altaProducto("2", "L", "no", "no", "ternera", "no", "si", "no");
			gestor.altaProducto("3", "S", "si", "si", "ternera", "no", "si", "si");
			gestor.listarProductos();
			break;
		case 5:
			//6- Buscar un elemento de Clase2.
			System.out.println(gestor.buscarProducto("3"));
			break;
		case 6:
			//7- Eliminar un elemento de Clase2 y listar. 
			gestor.eliminarProducto("1");
			gestor.listarProductos();
			break;
		case 7:
			//8- Lista elementos Clase2 por algún atributo
			gestor.listarPedidoAnno(2021);
			break;
		case 8:
			//10- Asignar un elemento que use Clase1 y Clase2 y listar.
			gestor.asignarCliente("854226125","2" );
			gestor.asignarCliente("985456845","3");
			break;
		case 9:
			//9- Listar elementos Clase2 con un elemento de Clase1
			gestor.listarProductoDeCliente("985456845");
			break;
		case 10:
			//11-Comprobar los pedidos realizados
			System.out.println(gestor.contadorDePedidosPorCliente("985456845"));
			break;
		case 11:
			//12-Ver el nivel del cliente
			gestor.nivelCliente("985456845");
			break;
		case 12:
			//13-Ver si nos toca un kebab gratis
			gestor.kebabGratis();
			break;
		case 13:
			//14-Canjear codigo
			gestor.canjearCodigo("985456845");
			break;
			
		default:
			System.out.println("Opcion seleccionada no definida");
			break;
		}
				
		} while (select!=14);
		
		
		
		
		
		
		
		
		
	


				



		
		
		
		
		
		

	}

}
