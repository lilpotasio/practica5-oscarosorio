package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class GestorPedidos {
	Scanner input= new Scanner(System.in);
	//variables
	String codigo;
	double random;
	int counter=0;
	int nivel=0;
	// (0,25 ptos) Atributos: ArrayList, de Clase1: listaClase1, ArrayList de Clase2: listaClase2
	private ArrayList<Producto> listaProductos;
	private ArrayList<Cliente> listaClientes;
	// (0,25 ptos) Un único constructor: no recibe nada e inicializa listaClase1 y listaClase2.
	public GestorPedidos() {
		listaProductos= new ArrayList<Producto>();
		listaClientes = new ArrayList<Cliente>();
	}
	// Método alta Clase1 (comprobando previamente si existe)
		public void altaCliente(String nMovil, String nombre, String calle, String numero) {
			if (!existeCliente(nMovil)) {
				Cliente nuevoCliente = new Cliente(nMovil, nombre);
				nuevoCliente.setCalle(calle);
				nuevoCliente.setNumero(numero);			
				nuevoCliente.setFechaAlta(LocalDate.now());
				listaClientes.add(nuevoCliente);
			} else {
				System.out.println("El cliente ya existe");
			}
		}
		// Método listar Clase1
		public void listaClientes() {
			for (Cliente cliente : listaClientes) {
				if (cliente != null) {
					System.out.println(cliente);
				}
			}
		}
		
		//metodo listar Clase2
		public void listarProductos() {
			for (Producto producto : listaProductos) {
				if (producto!= null) {
					System.out.println(producto);
				}
			}
		}
		// metodo existeCliente
		public boolean existeCliente(String nMovil) {
			for (Cliente cliente : listaClientes) {
				if (cliente != null && cliente.getnMovil().equals(nMovil)) {
					return true;
				}
			}
			return false;
		}
		// metodo Clase2
		public void altaProducto(String n_pedido,String tamano,String refrescoLata,String patatas,String tipoCarne,String queso,
				String salsaBlanca,String salsaPicante) {
			Producto nuevoProducto = new Producto(n_pedido);
			nuevoProducto.setTamano(tamano);
			nuevoProducto.setRefrescoLata(refrescoLata);
			nuevoProducto.setPatatas(patatas);
			nuevoProducto.setTipoCarne(tipoCarne);
			nuevoProducto.setQueso(queso);
			nuevoProducto.setSalsaBlanca(salsaBlanca);
			nuevoProducto.setSalsaPicante(salsaPicante);
			nuevoProducto.setFecha(LocalDate.now());
			listaProductos.add(nuevoProducto);
		}

		// metodo eliminar Clase2
		public void eliminarProducto(String nPedido) {
			Iterator<Producto> iteradorProductos = listaProductos.iterator();

			while (iteradorProductos.hasNext()) {
				Producto producto = iteradorProductos.next();
				if (producto.getnPedido().equals(nPedido)) {
					iteradorProductos.remove();
				}
			}

		}
		// metodo eliminar Clase1
				public void eliminarCliente(String nMovil) {
					Iterator<Cliente> iteradorClientes = listaClientes.iterator();
					while (iteradorClientes.hasNext()) {
						Cliente cliente = iteradorClientes.next();
						if (cliente.getnMovil().equals(nMovil)) {
							iteradorClientes.remove();
						}
					}

				}

		// metodo buscar elemento Clase1
		public Cliente buscarCliente(String nMovil) {
			for (Cliente cliente : listaClientes) {
				if (cliente != null && cliente.getnMovil().equals(nMovil)) {
					return cliente;
				}
			}
			return null;
		}

		// metodo buscar elemento Clase2
		public Producto buscarProducto(String nPedido) {
			for (Producto producto : listaProductos) {
				if (producto != null && producto.getnPedido().equals(nPedido)) {
					return producto;
				}
			}
			return null;
		}

		//Método asignar elemento Clase1 a Clase2
		public void asignarCliente(String nMovil, String nPedido) {
			if (buscarCliente(nMovil) != null && buscarProducto(nPedido) != null) {
				Cliente cliente = buscarCliente(nMovil);
				Producto producto = buscarProducto(nPedido);
				producto.setClienteProducto(cliente);
			}

		}
		// Método listar Clase2 por algún atributo
		public void listarPedidoAnno(int anno) {
			for (Producto producto : listaProductos) {
				if (producto.getFecha().getYear() == anno) {
					System.out.println(producto);
				}
			}

		}

		// metodo listarProductoDeCliente
		public void listarProductoDeCliente(String nMovil) {
			for (Producto producto : listaProductos) {
				if (producto.getClienteProducto() != null && producto.getClienteProducto().getnMovil().equals(nMovil)) {
					System.out.println(producto);
					counter++;
				}
			}

		}
		
		// metodo listarClientePorProducto
				public void listarClientePorProducto(String nPedido) {
					Producto producto = buscarProducto(nPedido);
					if (producto!=null) {
						System.out.println(producto.getClienteProducto());
					}
				}
				
				
				
		//metodo contarPedidos
				public int contadorDePedidosPorCliente(String nMovil) {
					counter=0;
					for (Producto producto : listaProductos) {
						if (producto.getClienteProducto() != null && producto.getClienteProducto().getnMovil().equals(nMovil)) {
							counter++;
						}
					}
					return counter;
				}
		//metodo nDePedidosPorCliente
				public void nDePedidosPorCliente(String nMovil) {					
					System.out.println("Has realizado "+contadorDePedidosPorCliente(nMovil)+" pedidos");
				}
		//metodo nivelCliente
				public void nivelCliente(String nMovil) {
					nivel=0;
					nivel=contadorDePedidosPorCliente(nMovil)/5;
					switch (nivel) {
					case 0:
						
						break;
					case 1:
						System.out.println("FELICIDADES ERES CLIENTE NUEVARDO");
						break;
					case 2:
						System.out.println("FELICIDADES ERES CLIENTE BUENARDO");
						break;
					case 3:
						System.out.println("FELICIDADES ERES CLIENTE REBUENARDO");
						break;
					case 4:
						System.out.println("FELICIDADES ERES CLIENTE SUPER REBUERNARDO");
						break;
					case 5:
						System.out.println("FELICIDADES ERES CLIENTE SUPER HIPER REBUERNARDO");
						break;
					case 6:
						System.out.println("FELICIDADES ERES CLIENTE SUPER HIPER MEGA REBUERNARDO");
						break;
					default:
						System.out.println("ESTAS ENFERMO ,POR TU SALUD DEJA DE CONSUMIR NUESTROS PRODUCTOS "
								+ "Y VISITA A STU MEDICO CUANTO ANTES, GRACIAS");
						break;
					}
				}
		//metodo kebabGratis
				public void kebabGratis() {
					random=Math.random();
					if (random%2==0) {
						System.out.println("FELICIDADES HAS RECIBIDO UN KEBAB GRATIS"
								      +  "\n        introduce el codigo:"
								      +  "\n            FREEKEBAB");
					} else {
						System.out.println("LO SENTIMOS PERO NO HAY KEBAB GRATIS PARA TI HOY");
					}
				}
		//metodo activarCodigo
				public void canjearCodigo(String nMovil) {
					System.out.println("Introduce el código");
					codigo = input.nextLine();
					if (codigo.compareToIgnoreCase("FREEKEBAB")==0) {
						System.out.println("Desea que se le devuelva el importe de su ultimo pedido?: (si/no)");
						for (Producto producto : listaProductos) {
							if (producto.getClienteProducto() != null && producto.getClienteProducto().getnMovil().equals(nMovil)) {
								System.out.println(producto);
								break;
							}
						}
						codigo = input.nextLine();
						switch (codigo) {
						case "si":
							System.out.println("SU DINERO HA SIDO DEVUELTO");
							break;
						case "no":
							System.out.println("SU DINERO NO HA SIDO DEVUELTO");
							break;

						default:
							System.out.println("ALERTA ERROR");
							break;
						}
					} else {
						System.out.println("CODIGO NO VALIDO");
					}
				}

}

