package clases;

import java.time.LocalDate;

public class Producto {

	//atributos
	String nPedido;
	String tamano;
	String refrescoLata;
	String patatas;
	String tipoCarne;
	String queso;
	String salsaBlanca;
	String salsaPicante;
	LocalDate fecha;
	Cliente clienteProducto;
	
	//constructor
	public Producto(String nPedido) {
		this.nPedido = nPedido;
	}
	//setter y getter
	
	public String getnPedido() {
		return nPedido;
	}

	public Cliente getClienteProducto() {
		return clienteProducto;
	}

	public void setClienteProducto(Cliente clienteProducto) {
		this.clienteProducto = clienteProducto;
	}

	public void setnPedido(String nPedido) {
		this.nPedido = nPedido;
	}

	public String getTamano() {
		return tamano;
	}

	public void setTamano(String tamano) {
		this.tamano = tamano;
	}

	public String getRefrescoLata() {
		return refrescoLata;
	}

	public void setRefrescoLata(String refrescoLata) {
		this.refrescoLata = refrescoLata;
	}

	public String getPatatas() {
		return patatas;
	}

	public void setPatatas(String patatas) {
		this.patatas = patatas;
	}

	public String getTipoCarne() {
		return tipoCarne;
	}

	public void setTipoCarne(String tipoCarne) {
		this.tipoCarne = tipoCarne;
	}

	public String getQueso() {
		return queso;
	}

	public void setQueso(String queso) {
		this.queso = queso;
	}

	public String getSalsaBlanca() {
		return salsaBlanca;
	}

	public void setSalsaBlanca(String salsaBlanca) {
		this.salsaBlanca = salsaBlanca;
	}

	public String getSalsaPicante() {
		return salsaPicante;
	}

	public void setSalsaPicante(String salsaPicante) {
		this.salsaPicante = salsaPicante;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}
	//tostring
	@Override
	public String toString() {
		return "Producto [nPedido=" + nPedido + ", tamano=" + tamano + ", refrescoLata=" + refrescoLata + ", patatas="
				+ patatas + ", tipoCarne=" + tipoCarne + ", queso=" + queso + ", salsaBlanca=" + salsaBlanca
				+ ", salsaPicante=" + salsaPicante + ", fecha=" + fecha + ", clienteTrabajo=" + clienteProducto + "]";
	}

}
