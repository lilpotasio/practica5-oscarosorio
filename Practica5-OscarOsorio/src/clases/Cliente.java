package clases;

import java.time.LocalDate;

public class Cliente {
	//atributos
	String nombre;
	String nMovil;
	String calle;
	String numero;
	LocalDate fechaAlta;
	//construcctor
	public Cliente(String nombre, String nMovil) {
		this.nombre = nombre;
		this.nMovil = nMovil;
	}
	//getter y setter
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getnMovil() {
		return nMovil;
	}
	public void setnMovil(String nMovil) {
		this.nMovil = nMovil;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public LocalDate getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(LocalDate fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	
	
	
	
	
	
	
	//to string
	
	@Override
	public String toString() {
		return "Cliente [nombre=" + nombre + ", nMovil=" + nMovil + ", calle=" + calle + ", numero=" + numero
				+ ", fechaAlta=" + fechaAlta + ", productoCliente=" + "]";
	}
	
	
	
	

}
